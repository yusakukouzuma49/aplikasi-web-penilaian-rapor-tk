<!DOCTYPE html>
<html>
    <head>
        <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        />
        <title>Input Nilai</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    </head>
    <link rel="stylesheet" href="Style1.css">
    <body>
    <div class="wrapper">
            <div class="section">
              <div class="top_navbar">
                <div class="hamburger">
                  <a href="#">
                    <i class="fas fa-bars"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="sidebar">
              <div class="profile">
                <h3>Main Menu</h3>
              </div>
              <ul>
                <li>
                  <a href="Home.php">
                    <span class="icon"><i class="fas fa-home"></i></span>
                    <span class="item">Home</span>
                  </a>
                </li>
                <li>
                  <a href="Data Guru.php">
                    <span class="icon"><i class="fas fa-chalkboard-teacher"></i></span>
                    <span class="item">Data Guru</span>
                  </a>
                </li>
                <li>
                  <a href="Data Murid.php">
                    <span class="icon"><i class="fas fa-user-friends"></i></span>
                    <span class="item">Data Murid</span>
                  </a>
                </li>
                <li>
                  <a href="Input Nilai.php">
                    <span class="icon"><i class="fas fa-tachometer-alt"></i></span>
                    <span class="item">Input Nilai</span>
                  </a>
                </li>
                <li>
                  <a href="Raport.php">
                    <span class="icon"><i class="fas fa-book"></i></span>
                    <span class="item">Raport</span>
                  </a>
                </li>
                <li>
                  <a href="Login.php">
                    <span class="icon"><i class="fas fa-sign-out-alt"></i></span>
                    <span class="item">Logout</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <main>
            <div>
                <h1 align="center">Input Nilai</h1><br>
                <?= (isset($_GET['pesan'])&& !empty($_GET['pesan'])) ? "<i>".$_GET['pesan']."</i>" : ""; ?>
                <br>
                <table class="table tabwrapper">
                    <tr>
                        <td><b>Nama</b></td>
                        <td align="center"><b>Nomor Induk</b></td>
                        <td align="center" colspan="2"><b>Aksi</b></th>
                    </tr>

                    <?php
                    include "koneksi.php";

                    $query = "SELECT * FROM murid";

                    $hasil_query = mysqli_query($koneksi, $query);

                    while($data = mysqli_fetch_assoc($hasil_query)): ?>
                    <tr>
                        <td><?=$data['nama']; ?></td>
                        <td align="center"><?=$data['ni']; ?></td>
                        <td><a href="inputdatanilai.php?ni=<?=$data['ni']; ?>" class="SpawnButton">Input</td>
                    </tr>
                    <?php endwhile; ?>
                </table>
                <br>
            </div>
          </main>
          
        <script src="./logic.js"></script>
        <nav>
          <div class="bawah">
            <p>Aplikasi Web Penilaian Raport TK</p>
            <p align="center">-------------------</p>
            <P>Created by Yusuf Kusuma</P>
          </div>
        </nav>
</html>