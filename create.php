<!DOCTYPE html>
<html>
    <head>
        <title>TAMBAH DATA MURID</title>
    </head>
    <link rel="stylesheet" href="Style1.css">
    <body>
        <form method="POST" action="store.php">
            <br>
            <h1 align="center">Tambah Data Murid</h1>
            <br>
            <table class="table">
                <tr>
                    <td>Nomor Induk</td>
                    <td><input type="text" name="ni" value="<?= rand(100,999); ?>" readonly class="input"></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td><input type="text" name="nama" value="" required class="input"></td>
                </tr>
                <tr>
                    <td>Usia</td>
                    <td><input type="text" name="usia" value="" required class="input"></td>
                </tr>
                <tr>
                    <td>Wali Murid</td>
                    <td><input type="text" name="wm" value="" required class="input"></td>
                </tr>
                <tr>
                    <td>Tanggal Lahir</td>
                    <td><input type="text" name="tl" value="" required class="input"></td>
                </tr>
            </table>
            <br>

            <div align="center">
                <button type="submit" class="tombol">Simpan</button>
                <a href="Data Murid.php" type="button" class="tombol">Kembali</a>
            </div>
        </form>
    </body>
</html>