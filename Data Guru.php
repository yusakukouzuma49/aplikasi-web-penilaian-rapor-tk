<!DOCTYPE html>
<html>
    <head>
        <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        />
        <title>Data Guru</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    </head>
    <link rel="stylesheet" href="Style1.css">
    <body>
    <div class="wrapper">
            <div class="section">
              <div class="top_navbar">
                <div class="hamburger">
                  <a href="#">
                    <i class="fas fa-bars"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="sidebar">
              <div class="profile">
                <h3>Main Menu</h3>
              </div>
              <ul>
                <li>
                  <a href="Home.php">
                    <span class="icon"><i class="fas fa-home"></i></span>
                    <span class="item">Home</span>
                  </a>
                </li>
                <li>
                  <a href="Data Guru.php">
                    <span class="icon"><i class="fas fa-chalkboard-teacher"></i></span>
                    <span class="item">Data Guru</span>
                  </a>
                </li>
                <li>
                  <a href="Data Murid.php">
                    <span class="icon"><i class="fas fa-user-friends"></i></span>
                    <span class="item">Data Murid</span>
                  </a>
                </li>
                <li>
                  <a href="Input Nilai.php">
                    <span class="icon"><i class="fas fa-tachometer-alt"></i></span>
                    <span class="item">Input Nilai</span>
                  </a>
                </li>
                <li>
                  <a href="Raport.php">
                    <span class="icon"><i class="fas fa-book"></i></span>
                    <span class="item">Raport</span>
                  </a>
                </li>
                <li>
                  <a href="Login.php">
                    <span class="icon"><i class="fas fa-sign-out-alt"></i></span>
                    <span class="item">Logout</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          </div>
          <main>
            <h1 align="center">Data Guru</h1><br>

            <?= (isset($_GET['pesan'])&& !empty($_GET['pesan'])) ? "<i>".$_GET['pesan']."</i>" : ""; ?>
            <br>
            <table class="table tabwrapper">
                  <?php
                  include "koneksi.php";

                  $query = "SELECT * FROM guru";

                  $hasil_query = mysqli_query($koneksi, $query);

                  while($data = mysqli_fetch_assoc($hasil_query)): ?>
                  <tr>
                      <td>NUPTK</td>
                      <td>:</td>
                      <td><?=$data['NUPTK']; ?></td>
                  </tr>
                  <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td><?=$data['nama']; ?></td>
                  </tr>
                  <tr>
                      <td>Gelar</td>
                      <td>:</td>
                      <td><?=$data['gelar']; ?></td>
                  </tr>
                  <tr>
                      <td>NIP</td>
                      <td>:</td>
                      <td><?=$data['nip']; ?></td>
                  </tr>
                  <tr>
                      <td>Status</td>
                      <td>:</td>
                      <td><?=$data['setatus']; ?></td>
                  </tr>
                  <tr>
                      <td>Golongan Darah</td>
                      <td>:</td>
                      <td><?=$data['goldarah']; ?></td>
                  </tr>
                  <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td><?=$data['jk']; ?></td>
                  </tr>
                  <tr>
                      <td>Tempat/Tanggal Lahir</td>
                      <td>:</td>
                      <td><?=$data['tempat']; ?>/<?=$data['tanggal']; ?></td>
                  </tr>
                  <tr>
                      <td>Agama</td>
                      <td>:</td>
                      <td><?=$data['agama']; ?></td>
                  </tr>
                  
                </tr>
               
                <?php endwhile; ?>
            </table>
            <br>
            </main>
          
          
        <script src="./logic.js"></script>
        <nav>
          <div class="bawah">
            <p>Aplikasi Web Penilaian Raport TK</p>
            <p align="center">-------------------</p>
            <P>Created by Yusuf Kusuma</P>
          </div>
      </nav>
    </body>
</html>