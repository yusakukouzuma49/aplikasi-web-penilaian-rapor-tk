<!DOCTYPE html>
<html>
    <head>
        <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        />
        <title>Hasil Raport</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    </head>
    <link rel="stylesheet" href="Style1.css">
    <body>
        <div class="wrapper">
            <div class="section">
              <div class="top_navbar">
                <div class="hamburger">
                  <a href="#">
                    <i class="fas fa-bars"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="sidebar">
              <div class="profile">
                <h3>Main Menu</h3>
              </div>
              <ul>
                <li>
                  <a href="Home.php">
                    <span class="icon"><i class="fas fa-home"></i></span>
                    <span class="item">Home</span>
                  </a>
                </li>
                <li>
                  <a href="Data Guru.php">
                    <span class="icon"><i class="fas fa-chalkboard-teacher"></i></span>
                    <span class="item">Data Guru</span>
                  </a>
                </li>
                <li>
                  <a href="Data Murid.php">
                    <span class="icon"><i class="fas fa-user-friends"></i></span>
                    <span class="item">Data Murid</span>
                  </a>
                </li>
                <li>
                  <a href="Input Nilai.php">
                    <span class="icon"><i class="fas fa-tachometer-alt"></i></span>
                    <span class="item">Input Nilai</span>
                  </a>
                </li>
                <li>
                  <a href="Raport.php">
                    <span class="icon"><i class="fas fa-book"></i></span>
                    <span class="item">Raport</span>
                  </a>
                </li>
                <li>
                  <a href="Login.php">
                    <span class="icon"><i class="fas fa-sign-out-alt"></i></span>
                    <span class="item">Logout</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <main>
              <h1 align="center">Hasil Penilaian</h1><br>

              <?= (isset($_GET['pesan'])&& !empty($_GET['pesan'])) ? "<i>".$_GET['pesan']."</i>" : ""; ?>
              <br>
              <table class="table tabwrapper">
                  <tr>
                      <td><b>Nama</b></td>
                      <td><b>Nomor Induk</b></td>
                      <td><b>Etika</b></td>
                      <td><b>Membaca</b></td>
                      <td><b>Menulis</b></td>
                      <td><b>Kreatifitas</b></td>
                      <td><b>Hasil Raport</b></td>
                  </tr>

                  <?php
                  include "koneksi.php";

                  $query = "SELECT * FROM murid";

                  $hasil_query = mysqli_query($koneksi, $query);

                  while($data = mysqli_fetch_assoc($hasil_query)): ?>
                  <tr>
                      <td><?=$data['nama']; ?></td>
                      <td><?=$data['ni']; ?></td>
                      <td><?=$data['etika']; ?></td>
                      <td><?=$data['membaca']; ?></td>
                      <td><?=$data['menulis']; ?></td>
                      <td><?=$data['kreatif']; ?></td>
                      <td><a href="hasil raport.php?ni=<?=$data['ni']; ?>" class="SpawnButton">Tampilkan</td>
                  </tr>
                  <?php endwhile; ?>
              </table>
          </main>
          
        <script src="./logic.js"></script>
        <nav>
          <div class="bawah">
            <p>Aplikasi Web Penilaian Raport TK</p>
            <p align="center">-------------------</p>
            <P>Created by Yusuf Kusuma</P>
          </div>
        </nav>
</html>