<html>
    <head>
        <title>EDIT NILAI MURID</title>
    </head>
    <link rel="stylesheet" href="Style1.css">
    <body>
    <?php
        include "koneksi.php";

        $ni = $_GET['ni'];

        (isset($ni) && empty($ni)) ? header('location: Raport.php') : '';

        $query = "SELECT * FROM murid WHERE ni = $ni LIMIT 1";

        $hasil_query = mysqli_query($koneksi, $query);

        $data = mysqli_fetch_assoc($hasil_query);

        empty($data) ? header('location: Raport.php') : '';

        ?>

        <form method="POST" action="storenilai.php?ni=<?=$ni; ?>">
            <br>
            <h1 align="center">Edit Nilai Murid</h1>
            <br>
            <table class="table">
                <tr>
                    <td>Etika</td>
                    <td><input type="text" name="etika" value="<?=$data['etika']; ?>" required class="input"></td>
                </tr>
                <tr>
                    <td>Membaca</td>
                    <td><input type="text" name="membaca" value="<?=$data['membaca']; ?>" required class="input"></td>
                </tr>
                <tr>
                    <td>Menulis</td>
                    <td><input type="text" name="menulis" value="<?=$data['menulis']; ?>" required class="input"></td>
                </tr>
                <tr>
                    <td>Kreatifitas</td>
                    <td><input type="text" name="kreatif" value="<?=$data['kreatif']; ?>" required class="input"></td>
                </tr>
            </table>
            <br>
            <div align="center">
                <button type="submit" class="tombol">Simpan</button>
                <a href="Input Nilai.php" type="button" class="tombol">Kembali</a>
            </div>
        </form>
    </body>
</html>