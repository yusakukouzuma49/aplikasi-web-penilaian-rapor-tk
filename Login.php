<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <link rel="stylesheet" href="Login.css">
    <body>

        <div class="Container">
        <form action="./logika/auth-login.php" method="POST">
            <h1 align="center">Login</h1>
            <div class="UserPass" align="center"> 
                <br>
                <input type="text" name="username" placeholder="Masukan Username" required>
                <br>
                <label></label>
                <br>
                <input type="password" name="sandi" placeholder="Masukan Password" required>
                <br>
            </div>
            <br>
            
            <div align="center">
                    <button type="Submit" name="btn-login" class="Login">
                        <span class="icon"><i class="fa fa-sign-in"></i></span>
                        <span class="item">MASUK</span>
                    </button>
            </div>
        </form>
        </div>
    </body>
</html>