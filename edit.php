<html>
    <head>
        <title>EDIT DATA MURID</title>
    </head>
    <link rel="stylesheet" href="Style1.css">
    <body>
        <?php
        include "koneksi.php";

        $ni = $_GET['ni'];

        (isset($ni) && empty($ni)) ? header('location: Data Murid.php') : '';

        $query = "SELECT * FROM murid WHERE ni = $ni LIMIT 1";

        $hasil_query = mysqli_query($koneksi, $query);

        $data = mysqli_fetch_assoc($hasil_query);

        empty($data) ? header('location: Data Murid.php') : '';

        ?>

        <form method="POST" action="update.php?ni=<?=$ni; ?>">
            <br>
            <h1 align="center">Edit Data Murid</h1>
            <br>
            <table class="table">
                <tr>
                    <td>Nomor Induk</td>
                    <td><input type="text" name="ni" value="<?=$data['ni']; ?>" readonly class="input"></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td><input type="text" name="nama" value="<?=$data['nama']; ?>" required class="input"></td>
                </tr>
                <tr>
                    <td>Usia</td>
                    <td><input type="text" name="usia" value="<?=$data['usia']; ?>" required class="input"></td>
                </tr>
                <tr>
                    <td>Wali Murid</td>
                    <td><input type="text" name="wm" value="<?=$data['wm']; ?>" required class="input"></td>
                </tr>
                <tr>
                    <td>Tanggal Lahir</td>
                    <td><input type="text" name="tl" value="<?=$data['tl']; ?>" required class="input"></td>
                </tr>
            </table>
            <br>
            <div align="center">
                <button type="submit" class="tombol">Simpan</button>
                <a href="Data Murid.php" type="button" class="tombol">Kembali</a>
            </div>
        </form>
    </body>
</html>