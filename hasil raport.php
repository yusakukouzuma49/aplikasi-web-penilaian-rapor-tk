<!DOCTYPE html>
<html>
    <head>
        <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        />
        <title>Input Nilai</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    </head>
    <link rel="stylesheet" href="Style1.css">
    <body>
    <?php
        include "koneksi.php";

        $ni = $_GET['ni'];

        (isset($ni) && empty($ni)) ? header('location: hasil raport.php') : '';

        $query = "SELECT * FROM murid WHERE ni = $ni LIMIT 1";

        $hasil_query = mysqli_query($koneksi, $query);

        $data = mysqli_fetch_assoc($hasil_query);

        empty($data) ? header('location: hasil raport.php') : '';

        ?>
          <main>
            <div>
                <form>
                    <br>
                    <br>
                <h1 align="center">HASIL RAPORT</h1>
                <br>
                <br>
                <table class="tabin">
                    <tr>
                        <td>Nama</td>
                        <td><?=$data['nama']; ?></td>
                    </tr>
                    <tr>
                        <td>Nomor Induk</td>
                        <td><?=$data['ni']; ?></td>
                    </tr>
                </table>
                <br>
                <table class="table tabwrapper">
                            <td>NO</td>
                            <td>ASPEK PERKEMBANGAN</td>
                            <td align="center">HASIL PENILAIAN</td>
                            <td align="center">GRADE PENILAIAN</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>ETIKA</td>
                            <td><?=$data['etika']?></td>
                            <td>
                            <?php
                                if ($data['etika']>=90){
                                    echo "A";
                                } else if ($data['etika']>=74 && $data['etika']<=90){
                                    echo "B";
                                } else if ($data['etika']>=60 && $data['etika']<=75){
                                    echo "C";
                                } else if ($data['etika']>=33 && $data['etika']<=59){
                                    echo "D";
                                } else if ($data['etika']>=0 && $data['etika']<=32){
                                    echo "E";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>MEMBACA</td>
                            <td><?=$data['membaca']?></td>
                            <td>
                            <?php
                                if ($data['membaca']>=90){
                                    echo "A";
                                } else if ($data['membaca']>=74 && $data['membaca']<=90){
                                    echo "B";
                                } else if ($data['membaca']>=60 && $data['membaca']<=75){
                                    echo "C";
                                } else if ($data['membaca']>=33 && $data['membaca']<=59){
                                    echo "D";
                                } else if ($data['membaca']>=0 && $data['membaca']<=32){
                                    echo "E";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>MENULIS</td>
                            <td><?=$data['menulis'];?></td>
                            <td>
                            <?php
                                if ($data['menulis']>=90){
                                    echo "A";
                                } else if ($data['menulis']>=74 && $data['menulis']<=90){
                                    echo "B";
                                } else if ($data['menulis']>=60 && $data['menulis']<=75){
                                    echo "C";
                                } else if ($data['menulis']>=33 && $data['menulis']<=59){
                                    echo "D";
                                } else if ($data['menulis']>=0 && $data['menulis']<=32){
                                    echo "E";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>KREATIFITAS</td>
                            <td><?=$data['kreatif'];?></td>
                            <td>
                            <?php
                                if ($data['kreatif']>=90){
                                    echo "A";
                                } else if ($data['kreatif']>=74 && $data['kreatif']<=90){
                                    echo "B";
                                } else if ($data['kreatif']>=60 && $data['kreatif']<=75){
                                    echo "C";
                                } else if ($data['kreatif']>=33 && $data['kreatif']<=59){
                                    echo "D";
                                } else if ($data['kreatif']>=0 && $data['kreatif']<=32){
                                    echo "E";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>TOTAL</td>
                            <td>
                                <?php
                                $query = "SELECT (etika+membaca+menulis+kreatif) as total FROM murid WHERE ni = $ni";

                                $hasil_query = mysqli_query($koneksi, $query);
                        
                                while($data = mysqli_fetch_assoc($hasil_query)):
                                    echo $data['total'];
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($data['total']>=240){
                                    echo "<b>LULUS</b>";
                                } else if($data['total']<=239){
                                    echo "<b>TIDAK LULUS</b>";
                                }
                            endwhile;
                                ?>
                            </td>
                        </tr>
                    </table>
                    </form>
                <br>
                <div align="center"> 
                  <a href="Raport.php" class="tombol">
                    <span class="icon"><i class="fa fa-sign-in"></i></span>
                    <span class="item">Kembali</span>
                  </a>
                </div>
            </div>
          </main>
          
        <script src="./logic.js"></script>
        <nav>
          <div class="bawah">
            <p>Aplikasi Web Penilaian Raport TK</p>
            <p align="center">-------------------</p>
            <P>Created by Yusuf Kusuma</P>
          </div>
        </nav>
</html>